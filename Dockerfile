FROM alpine:latest

LABEL description="Docker container for building static sites with Hugo."
LABEL maintainer="Krzysztof Olszewski <hello@devopsengineer.me>"

# config
ENV HUGO_VERSION=0.71.1
ENV HUGO_TYPE=_extended

ENV HUGO_ID=hugo${HUGO_TYPE}_${HUGO_VERSION}

RUN apk add --update git asciidoctor libc6-compat libstdc++ bash \
    && apk upgrade \
    && apk add --no-cache ca-certificates curl openssh-client rsync build-base
 
ADD https://github.com/gohugoio/hugo/releases/download/v${HUGO_VERSION}/${HUGO_ID}_Linux-64bit.tar.gz /tmp
RUN tar -xf /tmp/${HUGO_ID}_Linux-64bit.tar.gz -C /tmp \
    && mkdir -p /usr/local/bin \
    && mv /tmp/hugo /usr/local/bin/hugo \
    && mkdir -p /usr/local/src \
    && cd /usr/local/src \
    && curl -L https://bin.equinox.io/c/dhgbqpS8Bvy/minify-stable-linux-amd64.tgz | tar -xz && \
    mv minify /usr/local/bin \
    && rm -rf /tmp/${HUGO_ID}_linux_amd64 \
    && rm -rf /tmp/${HUGO_ID}_Linux-64bit.tar.gz \
    && rm -rf /tmp/LICENSE.md \
    && rm -rf /tmp/README.md \
    && addgroup -Sg 1000 hugo \
    && adduser -Sg hugo -u 1000 -h /src hugo
# USER hugo:hugo
VOLUME /src
WORKDIR /src
EXPOSE 1313
